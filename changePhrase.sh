#!/bin/bash

#flaga błędu - nie byłem w stanie odnaleźć sposobu do ograniczenia ilości wyskakujących komunikatów do jednego, więc stosuję obejście z flagą
ErrorFlag=0

#argumenty pomocy do wywołania. zakłada się, że pomoc wyskoczy jedynie przy podaniu jednego parametru i ma nim być --help lub -h
if [ $# = 1 ] && ([ $1 = "--help" ] || [ $1 = "-h" ]) ; 
	then
	echo " Funkcja zamiany podanych fraz."
	echo " "
	echo " 1. Żeby uruchomić skrypt, musisz podać dwa argumenty oddzielone spacją. "
	echo " 2. Pierwszy argument jest wartością oryginalną, a drugi wartością docelową. "
	echo " 3. Zamiana frazy dotyczy plików obecnego folderu i jego podfolderów. "
	echo " 4. Przykładowe zapytanie wygląda następująco: "
	echo " "
	echo " sh changePhrase.sh 'Kali je banany' 'Kajtek lubi poziomki'"
	echo " "
	echo " 5. Skrypt jest wstępnie zabezpieczony przed modyfikacją przez samego siebie"
fi

#eliminacja problemu spacji w nazwie pliku
IFS="
"

#wyszukuje nam rekurencyjnie pliki (* wyszukałoby jedynie w obecnym katalogu) i przypisuje je do zmiennej f
for f in $(find . -type f); 
do
	
	#pamiętny moment umysłowego tour de france :)
	if [ $0 != "$f" ] && [ $0 != "./$f" ]; 
	then

		#jeżeli będą dokładnie dwa argumenty, skrypt wykona zamianę podanej frazy 1 na frazę 2
		if [ $# -eq 2 ]; 
			then
			echo "Zamiana $1 na $2 w $f" &&
			sed -i "{s|$1|$2|g}" "$f"

		#jeżeli będą więcej niż dwa parametry, wyskoczy nam error
		elif [ $# -gt 2 ] || [ $# = 0 ]; 
			then
			ErrorFlag=1
		fi
	fi
done

#obejście, żeby wyrzuciło nam jedynie jeden błąd dla całości, a nie dla każdego pliku, zasypując log śmieciem
if [ $ErrorFlag -gt 0 ];
then
	echo "ERROR - podaj dwa parametry lub spytaj o pomoc !"
	echo "Przykład prawidłowo sformułowanej komendy: "
	echo "sh changePhrase.sh 'Kali je banany' 'Kajtek lubi poziomki' "

fi
