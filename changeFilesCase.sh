#!/bin/bash

#ustalenie zmiennej do późniejszego wykorzystania
TARGET=0

#argumenty pomocy do wywołania. zakłada się, że pomoc wyskoczy jedynie przy podaniu jednego parametru i ma nim być --help lub -h.
if [ $# = 1 ] && ([ $1 = "--help" ] || [ $1 = "-h" ]) ; 
	then
	echo " Funkcja zamiany wielkości znaków w nazwach plików."
	echo " "
	echo " 1. Żeby uruchomić skrypt, musisz podać przynajmniej dwa argumenty oddzielone spacją. "
	echo " 	1.1 Argumenty określające zamianę to 'upper' dla dużych i 'lower' dla małych znaków. "
	echo " 2. Pierwszy argument jest wartością oryginalną, a drugi wartością docelową. "
	echo " 3. Trzeci argument jest opcjonalny i precyzuje plik którego ma dotyczyć zmiana"
	echo " 4. Zamiana znaków dotyczy domyślnie wszystkich plików bieżącego folderu "
	echo " 5. Przykładowe zapytanie ze sprecyzowaniem wygląda następująco: "
	echo " "
	echo " sh changePhrase.sh lower upper plik.txt"
	echo " "
	echo " 7. Skrypt jest wstępnie zabezpieczony przed modyfikacją przez samego siebie, chyba, że go wskażemy ręcznie trzecim parametrem"
fi

#eliminacja problemu spacji w nazwie pliku
IFS="
"

#jeżeli będzie 0 lub więcej niż trzy parametry, wyskoczy nam error
if [ $# -gt 3 ] || [ $# = 0 ]; 
	then
	echo "ERROR - podaj parametry lub spytaj o pomoc !"
	echo "Przykład prawidłowo sformułowanej komendy zamiany małych znaków na duże: "
	echo "sh changeFilesCase.sh lower upper"

#jeżeli są dwa parametry, to domyślnie wszystkie pliki na poziomie 1 (katalog bieżący) ulegają zmianie. Skrypt nie jest indeksowany(grep -v)!
elif [ $# -eq 2 ];
	then
	TARGET=$(find . -maxdepth 1 -type f | grep -v "^\./$0");

#jeżeli są trzy parametry, to trzeci jest naszym plikiem do zmiany. Każdy plik jest dozwolony!
elif [ $# -eq 3 ];
	then
	TARGET=$3;
fi

#jeżeli są dwa lub trzy parametry to wykonuje docelowe dzialanie skryptu
if [ $# = 2 ] || [ $# = 3 ];
	then
	for f in $TARGET; do

		#pamiętny moment umysłowego tour de france :)
		if [ $0 != "$f" ] && [ $0 != "./$f" ]; 
		then

			#zamiana małych znaków na duże + wywołanie co jest zamieniane w konsoli
			if [ $1 = "lower" ] && [ $2 = "upper" ] ; 
				then
				echo "Zamiana znaków małych na duże w nazwie pliku $f" &&
				mv "$f" "$(echo "$f" | tr '[:lower:]' '[:upper:]')";
			#zamiana dużych znaków na małe + wywołanie co jest zamieniane w konsoli
			elif [ $1 = "upper" ] && [ $2 = "lower" ] ; 
				then
				echo "Zamiana znaków dużych na małe w nazwie pliku $f" &&
				mv "$f" "$(echo "$f" | tr '[:upper:]' '[:lower:]')";
			fi
		fi
	done
fi
