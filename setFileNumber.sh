#!/bin/bash

#zmienna numeracyjna, wyznacza od jakiego numeru będziemy numerować pliki w pętli
i=1

#argumenty pomocy do wywołania. zakłada się, że pomoc wyskoczy jedynie przy podaniu jednego parametru i ma nim być --help lub -h.
if [ $# = 1 ] && ([ $1 = "--help" ] || [ $1 = "-h" ]) ; 
	then
	echo " Funkcja numerowania plików."
	echo " 1. Skrypt numeruje wszystkie pliki znajdujące się w tym samym katalogu "
	echo " 2. Niedozwolone jest stosowanie parametrów innych niż --help oraz -h "
	echo " 3. Skrypt wykona numerowanie jedynie przy zerowej liczbie parametrów"
	echo " 4. Skrypt celowo nie uwzględnia sam siebie w celu ochrony przed automodyfikacją!"
	echo " 5. Przykład poprawnego użycia skryptu: "
	echo "  sh setFileNumber.sh "
	echo " UWAGA - numeracja będzie się nakładać przy każdym kolejnym wykonaniu skryptu!"
fi

#eliminacja problemu spacji w nazwie pliku
IFS="
"

#jeżeli nie ma podanych parametrów to wyszukuje nam pliki inne niż skrypt i przypisuje je do zmiennej f, a następnie je nam numeruje
if [ $# = 0 ];
	then
	for f in $(find * -maxdepth 0 -type f); do
		
		#pamiętny moment umysłowego tour de france :)
		if [ $0 != "$f" ] && [ $0 != "./$f" ]; 
		then
			#basename pozwala na pozyskanie samej nazwy wybranego pliku, nie całej ścieżki. 
			name=$(basename $f)
			echo "Numerowanie pliku $f" &&
			mv "$f" "${i}_${name}"

			#przyrost zmiennej dla kolejnych wykonań pętli for
			i=$((i+1))
		fi
	done

#przy więcej niż jednym parametrze wyskakuje error i przykład użycia poprawnego skryptu
elif [ $# -gt 1 ];
	then
	echo "ERROR - nie podawaj parametrów!"
	echo "Jedyne dozwolone parametry to --help oraz -h, które odeślą ciebie do strony pomocy"
	echo "Przykład prawidłowo sformułowanej komendy numeracji plików: "
	echo "sh setFileNumber.sh"
fi
